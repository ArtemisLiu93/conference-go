import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()



def process_approvals(ch, method, properties, body):
    print("  Received %r" % body)

    message = json.loads(body)
    print(message)
    presenter_name = message["presenter_name"]
    presentation_title = message["title"]
    presenter_email = message["presenter_email"]

    send_mail(
        "Your presentation has been accepted",
        f"{presenter_name}, We're happy to tell you that your presentation {presentation_title} has been accepted",
        "admin@conference.go",
        [presenter_email],
        fail_silently=False,
    )

channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approvals,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):

    message = json.loads(body)
    presenter_name = message["presenter_name"]
    presentation_title = message["title"]
    presenter_email = message["presenter_email"]

    send_mail(
        "Your presentation has been rejected",
        f"{presenter_name}, We're sad to tell you that your presentation {presentation_title} has been rejected",
        "admin@conference.go",
        [presenter_email],
        fail_silently=False,
    )
channel.queue_declare(queue="presentation_reject")
channel.basic_consume(
    queue="presentation_reject",
    on_message_callback=process_rejection,
    auto_ack=True,
)

channel.start_consuming()
